# Template - Überschrift
Hier den Titel des Repositoriums eintragen

## Über dieses Projekt
Dieses Projekt ...

## Warum dieses Projekt?

## Lizenzierung
Dieses Projekt steht unter der [Creative Commons - Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de). Diese Lizenz erlaubt es dir, das Material zu teilen und weiterzuentwickeln, solange du den Urheber nennst.t.

## Wie kann ich mitmachen?
Wir freuen uns über jede Unterstützung! Du kannst dich auf folgende Arten einbringen:

- Erstellen ...
- Übersetzen und Anpassen von ...
- Verbessern und Aktualisieren vorhandener Ressourcen
- Teilen und Verbreiten von ... in deinem Netzwerk

Um loszulegen, schau dir unsere [Mitwirkungsrichtlinien](contributing.md) an und eröffne ein neues Issue oder sende uns einen Pull Request.

## Kontakt
Wenn du Fragen oder Anregungen hast, kontaktiere uns gerne z.B. über unser Issue-Tracker. ***<- Hyperlink zu Issues ergänzen!!!***

Vielen Dank für dein Interesse an unserem Projekt!